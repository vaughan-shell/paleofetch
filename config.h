#include "logos/alpine.h"
#define COLOR "\e[1;34m"

#define CONFIG \
{ \
   /* name            function                 cached */\
    { "  ",         get_os,                  true  }, \
    { "  ",       get_host,                true  }, \
    { "  ",     get_kernel,              true  }, \
    { "  ",    get_battery_percentage,  false }, \
    { "  ",      get_shell,             true }, \
    SPACER \
    { "",             get_colors1,             false }, \
}

#define CPU_CONFIG \
{ \
}

#define GPU_CONFIG \
{ \
}
