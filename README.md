My custom build of paleofetch.
In short, changed color to purple, changes os name type from regular to pretty name, changed ascii art to rocket ascii art, removed a lot of the default lines(outputs??idk what to call them), and turned on colors at the bottom of the prompt. Also switched out identifiers like OS, or UPTIME, to nerd font icons.

--------------
HOW TO INSTALL
--------------

DEPENDENCIES:

libX11 and libpci

example: To install these for gentoo, run either with sudo, doas, or as root:

"# emerge -av sys-apps/pciutils x11-libs/libX11"

PALEOFETCH:

once you have the dependencies installed, you can now install my build of paleofetch. 

run:

git clone https://github.com/vaughan-shell/paleofetch.git

cd paleofetch

chmod +x config_scripts/battery_config.sh #This makes the battery config script executable, which is necessary for building paleofetch

make

This will get you a built binary that you can execute with ./paleofetch in the paleofetch directory, but if you want to install to your /usr/local/bin directory, so that you can run it as any other command, run with either sudo, doas, or as root:

make clean install

And now you can run paleofetch by typing paleofetch into a terminal or however you wanna use it. 
